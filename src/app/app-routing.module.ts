import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './shared/service/auth-guard.service';

import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { AdminComponent } from './admin/admin.component';
import { TeamAddComponent } from './team/team-add/team-add.component';
import { TeamEditComponent } from './team/team-edit/team-edit.component';
import { TeamListComponent } from './team/team-list/team-list.component';
import { TravelogueAddComponent } from './travelogue/travelogue-add/travelogue-add.component';
import { TravelogueEditComponent } from './travelogue/travelogue-edit/travelogue-edit.component';
import { TravelogueListComponent } from './travelogue/travelogue-list/travelogue-list.component';
import { UserAddComponent } from './user/user-add/user-add.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { UserListComponent } from './user/user-list/user-list.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'signin',
    component: SigninComponent
  }, {
    path: 'signup',
    component: SignupComponent
  }, {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/teams',
    component: TeamListComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/teams/add',
    component: TeamAddComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/teams/:id/edit',
    component: TeamEditComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/travelogues',
    component: TravelogueListComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/travelogues/add',
    component: TravelogueAddComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/travelogues/:id/edit',
    component: TravelogueEditComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/users',
    component: UserListComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/users/add',
    component: UserAddComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: 'admin/users/:id/edit',
    component: UserEditComponent,
    canActivate: [AuthGuardService],
    data: { role: 'ROLE_ADMIN' }
  }, {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
