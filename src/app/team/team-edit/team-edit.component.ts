import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { TeamService } from 'src/app/shared/service/team.service';
import { TravelogueService } from 'src/app/shared/service/travelogue.service';
import { UserService } from 'src/app/shared/service/user.service';

import { Team } from 'src/app/shared/model/team.model';
import { User } from 'src/app/shared/model/user.model';

@Component({
    selector: 'app-team-edit',
    templateUrl: './team-edit.component.html',
    styleUrls: ['./team-edit.component.css'],
    providers: [TeamService, TravelogueService, UserService]
})
export class TeamEditComponent implements OnInit {
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    team: Team;
    allUsers: User[] = [];
    selectedUsers: string[] = [];
    allTravelogues: string[] = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private teamService: TeamService,
        private travelogueService: TravelogueService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');

        if (this.user_email && this.user_token) {
            this.route.paramMap.subscribe(params => {
                this.teamService.get(params['params'].id, this.user_token).subscribe(data => {
                    this.team = data;

                    for(let i = 0; i < this.team.users.length; i++) {
                        this.selectedUsers.push(this.team.users[i]['@id']);
                    }

                    this.userService.getAll(this.user_token).subscribe(data => {
                        this.allUsers = data['hydra:member'];

                        this.travelogueService.getAll(this.user_token).subscribe(data => {
                            this.allTravelogues = data['hydra:member'];
                            this.initLoading = false;
                        });
                    });
                });
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    onChangeUser($event, user) {
        if ($event.target.checked) {
            this.selectedUsers.push(user['@id']);
        } else {
            this.selectedUsers.splice(this.selectedUsers.indexOf(user['@id']), 1);
        }
    }

    editTeam() {
        this.actionLoading = true;

        let editedTeam = {
            'team_id': this.team.id,
            'name': this.team.name,
            'travelogue': this.team.travelogue['@id'],
            'users': this.selectedUsers
        }

        this.teamService.editTeam(editedTeam, this.user_token).subscribe(data => {
            this.team = data;
            this.actionLoading = false;
        });
    }
}
