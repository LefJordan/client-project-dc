import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TeamService } from 'src/app/shared/service/team.service';

import { Team } from 'src/app/shared/model/team.model';

@Component({
    selector: 'app-team-list',
    templateUrl: './team-list.component.html',
    styleUrls: ['./team-list.component.css'],
    providers: [TeamService]
})
export class TeamListComponent implements OnInit {
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    allTeams: Team[] = [];

    constructor(
        private router: Router,
        private teamService: TeamService
    ) { }    

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');
        
        if(this.user_email && this.user_token) {
            this.teamService.getAll(this.user_token).subscribe(data => {
                this.allTeams = data['hydra:member'];
                this.initLoading = false;
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    removeTeam(team) {
        this.actionLoading = true;

        this.teamService.deleteTeam(team.id, this.user_token).subscribe(data => {
            this.allTeams.splice(this.allTeams.indexOf(team), 1);
            this.actionLoading = false;
        });
    }
}