import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from 'src/app/shared/service/user.service';
import { TravelogueService } from 'src/app/shared/service/travelogue.service';
import { TeamService } from 'src/app/shared/service/team.service';

import { User } from 'src/app/shared/model/user.model';
import { Travelogue } from 'src/app/shared/model/travelogue.model';

@Component({
    selector: 'app-team-add',
    templateUrl: './team-add.component.html',
    styleUrls: ['./team-add.component.css'],
    providers: [UserService, TravelogueService, TeamService]
})
export class TeamAddComponent implements OnInit {
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    name: string = '';
    allUsers: User[] = [];
    allTravelogues: Travelogue[] = [];

    selectedUsers: string[] = [];
    selectedTravelogue: string = '';

    constructor(
        private router: Router,
        private userService: UserService,
        private travelogueService: TravelogueService,
        private teamService: TeamService
    ) { }    

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');
        
        if(this.user_email && this.user_token) {
            this.userService.getAll(this.user_token).subscribe(data => {
                this.allUsers = data['hydra:member'];

                this.travelogueService.getAll(this.user_token).subscribe(data => {
                    this.allTravelogues = data['hydra:member'];
                    this.initLoading = false;
                });
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    onChangeUser($event, user) {
        if ($event.target.checked) {
            this.selectedUsers.push(user['@id']);
        } else {
            this.selectedUsers.splice(this.selectedUsers.indexOf(user['@id']), 1);
        }
    }

    onChangeTravelogue(IRIvalue) {
        this.selectedTravelogue = IRIvalue;
    }

    createTeam(teamForm) {
        if(this.selectedTravelogue != '') {
            this.actionLoading = true;

            let newTeam = {
                'name': this.name,
                'travelogue': this.selectedTravelogue,
                'users': this.selectedUsers
            }

            this.teamService.addTeam(newTeam, this.user_token).subscribe(data => {
                teamForm.reset();

                this.actionLoading = false;
            });
        }
    }
}
