import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../shared/service/auth.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.css'],
    providers: [AuthService]
})
export class SigninComponent implements OnInit {
    actionLoading: boolean = false;
    hasErrors: boolean = false;
    errorMessage: string = '';

    email: string = '';
    password: string = '';

    constructor(
        private router: Router,
        private authService: AuthService
    ) { }    

    ngOnInit() {
    
    }

    signin() {
        this.actionLoading = true;
        this.hasErrors = false;

        let userCredentials = {
            'email': this.email,
            'password': this.password
        }

        this.authService.login(userCredentials).subscribe(
            data => {
                localStorage.setItem('user_email', userCredentials.email);
                localStorage.setItem('user_token', 'Bearer ' + data.token);

                this.router.navigate(['/']);
            },
            error => {
                this.actionLoading = false;
                this.hasErrors = true;

                if(error.code == 401) {
                    this.errorMessage = 'Identifiants incorrects';
                } else {
                    this.errorMessage = 'Une erreur est survenue';
                }
            }
        );
    }
}
