import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import config from '../../assets/config.json';

import { UserService } from '../shared/service/user.service';
import { TeamService } from '../shared/service/team.service';
import { TravelogueService } from '../shared/service/travelogue.service';

import { User } from '../shared/model/user.model';
import { Team } from '../shared/model/team.model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers: [UserService, TeamService, TravelogueService]
})
export class HomeComponent implements OnInit {
    apiUrl = config.apiUrl;
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    currentUser: User;
    teamsOfUser: Team[] = [];
    traveloguesOfUser: Team[] = [];

    constructor(
        private router: Router,
        private userService: UserService,
        private teamService: TeamService,
        private travelogueService: TravelogueService,
    ) { }    

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');

        if(this.user_email && this.user_token) {
            this.userService.getByEmail(this.user_email, this.user_token).subscribe(
                data => { 
                    this.currentUser = data['hydra:member'][0];

                    this.teamService.getByUser(this.currentUser.id, this.user_token).subscribe(
                        data => {
                            this.teamsOfUser = data['hydra:member'];
                            
                            this.travelogueService.getByUser(this.currentUser.id, this.user_token).subscribe(
                                data => {
                                    this.traveloguesOfUser = data ['hydra:member'];
                                    this.initLoading = false;
                                }
                            );
                        }
                    );
                },
                error => {
                    this.router.navigate(['/signin']);
                }
            )
        } else {
            this.router.navigate(['/signin']);
        }
    }
}
