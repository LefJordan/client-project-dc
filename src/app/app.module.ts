import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AuthGuardService } from './shared/service/auth-guard.service';
import { UserService } from './shared/service/user.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';
import { AdminComponent } from './admin/admin.component';
import { TeamAddComponent } from './team/team-add/team-add.component';
import { TeamEditComponent } from './team/team-edit/team-edit.component';
import { TeamListComponent } from './team/team-list/team-list.component';
import { TravelogueAddComponent } from './travelogue/travelogue-add/travelogue-add.component';
import { TravelogueEditComponent } from './travelogue/travelogue-edit/travelogue-edit.component';
import { TravelogueListComponent } from './travelogue/travelogue-list/travelogue-list.component';
import { UserAddComponent } from './user/user-add/user-add.component';
import { UserEditComponent } from './user/user-edit/user-edit.component';
import { UserListComponent } from './user/user-list/user-list.component';
import { ActionLoaderComponent } from './action-loader/action-loader.component';
import { InitLoaderComponent } from './init-loader/init-loader.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SigninComponent,
    SignupComponent,
    AdminComponent,
    TeamAddComponent,
    TeamEditComponent,
    TeamListComponent,
    TravelogueAddComponent,
    TravelogueEditComponent,
    TravelogueListComponent,
    UserAddComponent,
    UserEditComponent,
    UserListComponent,
    ActionLoaderComponent,
    InitLoaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AuthGuardService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
