import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { TeamService } from 'src/app/shared/service/team.service';
import { TravelogueService } from 'src/app/shared/service/travelogue.service';
import { FileUploadService } from 'src/app/shared/service/file-upload.service';

import { Team } from 'src/app/shared/model/team.model';

@Component({
    selector: 'app-travelogue-add',
    templateUrl: './travelogue-add.component.html',
    styleUrls: ['./travelogue-add.component.css'],
    providers: [TeamService, TravelogueService, FileUploadService]
})
export class TravelogueAddComponent implements OnInit {
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    name: string = '';
    description: string = '';
    PDFFormData: FormData;
    selectedPDFName: string = 'Choisir un PDF';
    allTeams: Team[] = [];
    selectedTeams: string[] = [];

    constructor(
        private router: Router,
        private teamService: TeamService,
        private travelogueService: TravelogueService,
        private fileUploadService: FileUploadService
    ) { }

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');

        if (this.user_email && this.user_token) {
            this.teamService.getAll(this.user_token).subscribe(data => {
                this.allTeams = data['hydra:member'];
                this.initLoading = false;
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    onChangeTeam($event, team) {
        if ($event.target.checked) {
            this.selectedTeams.push(team['@id']);
        } else {
            this.selectedTeams.splice(this.selectedTeams.indexOf(team['@id']), 1);
        }
    }

    onChangePDF(file) {
        if (file && file.type == 'application/pdf') {
            this.selectedPDFName = file.name;
            this.PDFFormData = new FormData();

            this.PDFFormData.append('file', file, file.name);
        } else {
            console.log('error type');
        }
    }

    createTravelogue(travelogueForm) {
        if(this.PDFFormData) {
            this.actionLoading = true;

            this.fileUploadService.addFile(this.PDFFormData, this.user_token).subscribe(data => {
                let newTravelogue = {
                    'name': this.name,
                    'description': this.description,
                    'pdfName': this.selectedPDFName,
                    'pdfLink': data['@id'],
                    'teams': this.selectedTeams
                }

                this.travelogueService.addTravelogue(newTravelogue, this.user_token).subscribe(data => {
                    travelogueForm.reset();
        
                    this.actionLoading = false;
                });
            });
        }
    }
}
