
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import config from '../../../assets/config.json';

import { TeamService } from 'src/app/shared/service/team.service';
import { TravelogueService } from 'src/app/shared/service/travelogue.service';
import { FileUploadService } from 'src/app/shared/service/file-upload.service';
import { MailService } from 'src/app/shared/service/mail.service';

import { Team } from 'src/app/shared/model/team.model';
import { Travelogue } from 'src/app/shared/model/travelogue.model';
import { Mail } from 'src/app/shared/model/mail.model.js';

@Component({
    selector: 'app-travelogue-edit',
    templateUrl: './travelogue-edit.component.html',
    styleUrls: ['./travelogue-edit.component.css'],
    providers: [TeamService, TravelogueService, FileUploadService, MailService]
})
export class TravelogueEditComponent implements OnInit {
    apiUrl = config.apiUrl;
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    travelogue: Travelogue;
    newPDFFormData: FormData;
    selectedPDFName: string = 'Choisir un PDF';
    allTeams: Team[] = [];
    selectedTeams: string[] = [];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private teamService: TeamService,
        private travelogueService: TravelogueService,
        private fileUploadService: FileUploadService,
        private mailService: MailService
    ) { }

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');

        if (this.user_email && this.user_token) {
            this.route.paramMap.subscribe(params => {
                this.travelogueService.get(params['params'].id, this.user_token).subscribe(data => {
                    this.travelogue = data;

                    for (let i = 0; i < this.travelogue.teams.length; i++) {
                        this.selectedTeams.push(this.travelogue.teams[i]['@id']);
                    }

                    this.teamService.getAll(this.user_token).subscribe(data => {
                        this.allTeams = data['hydra:member'];

                        this.initLoading = false;
                    });
                });
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    onChangeTeam($event, team) {
        if ($event.target.checked) {
            this.selectedTeams.push(team['@id']);
        } else {
            this.selectedTeams.splice(this.selectedTeams.indexOf(team['@id']), 1);
        }
    }

    onChangePDF(file) {
        if (file && file.type == 'application/pdf') {
            this.selectedPDFName = file.name;
            this.newPDFFormData = new FormData();

            this.newPDFFormData.append('file', file, file.name);
        } else {
            console.log('error type');
        }
    }

    sendTravelogue() {
        this.actionLoading = true;
        let mail_request = { 'mails': [] };

        for (let i = 0; i < this.travelogue.teams.length; i++) {
            for (let j = 0; j < this.travelogue.teams[i].users.length; j++) {
                let mail = new Mail();

                mail.subject = 'Carnet de voyage : ' + this.travelogue.name;

                mail.mail_to = this.travelogue.teams[i].users[j].email;
                mail.msg_html = '<html><head></head><body><p>Bonjour,</p><p>Détails du carnet de voyage : </p><h4>' + this.travelogue.name + '</h4><p>' + this.travelogue.description + '</p><a href="' + this.apiUrl + '/media/' + this.travelogue.pdfLink.filePath + '">Voir le PDF</a></body></html>';
                mail.msg_txt = '';

                mail_request.mails.push(mail);
            }
        }

        this.mailService.sendMultiple(mail_request, this.user_token).subscribe(data => {
            this.actionLoading = false;
        });
    }

    editTravelogue() {
        this.actionLoading = true;

        let editedTravelogue = {
            'travelogue_id': this.travelogue.id,
            'name': this.travelogue.name,
            'description': this.travelogue.description,
            'teams': this.selectedTeams
        }

        if (this.newPDFFormData) {
            this.fileUploadService.addFile(this.newPDFFormData, this.user_token).subscribe(data => {

                editedTravelogue['pdfName'] = this.selectedPDFName;
                editedTravelogue['pdfLink'] = data['@id'];

                this.travelogueService.editTravelogue(editedTravelogue, this.user_token).subscribe(data => {
                    let newTravelogue = data;

                    this.fileUploadService.deleteFile(this.travelogue.pdfLink.id, this.user_token).subscribe(data => {
                        this.travelogue = newTravelogue;

                        this.actionLoading = false;
                    });
                });
            });
        } else {
            this.travelogueService.editTravelogue(editedTravelogue, this.user_token).subscribe(data => {
                this.travelogue = data;
                this.actionLoading = false;
            });
        }
    }
}
