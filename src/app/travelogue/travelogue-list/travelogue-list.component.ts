import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import config from '../../../assets/config.json';

import { TravelogueService } from 'src/app/shared/service/travelogue.service';

import { Travelogue } from 'src/app/shared/model/travelogue.model';

@Component({
    selector: 'app-travelogue-list',
    templateUrl: './travelogue-list.component.html',
    styleUrls: ['./travelogue-list.component.css'],
    providers: [TravelogueService]
})
export class TravelogueListComponent implements OnInit {
    apiUrl = config.apiUrl;
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    allTravelogues: Travelogue[] = [];

    constructor(
        private router: Router,
        private travelogueService: TravelogueService
    ) { }    

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');
        
        if(this.user_email && this.user_token) {
            this.travelogueService.getAll(this.user_token).subscribe(data => {
                this.allTravelogues = data['hydra:member'];
                this.initLoading = false;
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    removeTravelogue(travelogue) {
        this.actionLoading = true;

        this.travelogueService.deleteTravelogue(travelogue.id, this.user_token).subscribe(data => {
            this.allTravelogues.splice(this.allTravelogues.indexOf(travelogue), 1);
            this.actionLoading = false;
        });
    }
}
