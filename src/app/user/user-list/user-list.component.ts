import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from 'src/app/shared/service/user.service';

import { User } from 'src/app/shared/model/user.model';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css'],
    providers: [UserService]
})
export class UserListComponent implements OnInit {
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    allUsers: User[] = [];

    constructor(
        private router: Router,
        private userService: UserService
    ) { }    

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');
        
        if(this.user_email && this.user_token) {
            this.userService.getAll(this.user_token).subscribe(data => {
                this.allUsers = data['hydra:member'];
                this.initLoading = false;
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    removeUser(user) {
        this.actionLoading = true;

        this.userService.deleteUser(user.id, this.user_token).subscribe(data => {
            this.allUsers.splice(this.allUsers.indexOf(user), 1);
            this.actionLoading = false;
        });
    }
}
