import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { UserService } from 'src/app/shared/service/user.service';

import { User } from 'src/app/shared/model/user.model';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.css'],
    providers: [UserService]
})
export class UserEditComponent implements OnInit {
    actionLoading: boolean = false;
    initLoading: boolean = true;
    user_email: string = '';
    user_token: string = '';

    user: User;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.user_email = localStorage.getItem('user_email');
        this.user_token = localStorage.getItem('user_token');

        if (this.user_email && this.user_token) {
            this.route.paramMap.subscribe(params => {
                this.userService.get(params['params'].id, this.user_token).subscribe(data => {
                    this.user = data;

                    this.initLoading = false;
                });
            });
        } else {
            this.router.navigate(['/signin']);
        }
    }

    onChangeRole($event) {
        if ($event.target.checked) {
            this.user.roles.push('ROLE_ADMIN');
        } else {
            this.user.roles.splice(this.user.roles.indexOf('ROLE_ADMIN'), 1);
        }
    }

    editUser() {
        this.actionLoading = true;

        let editedUser = {
            'user_id': this.user.id,
            'email': this.user.email,
            'firstname': this.user.firstname,
            'lastname': this.user.lastname,
            'roles': this.user.roles
        }

        this.userService.editUser(editedUser, this.user_token).subscribe(data => {
            this.user = data;
            this.actionLoading = false;
        });

    }
}
