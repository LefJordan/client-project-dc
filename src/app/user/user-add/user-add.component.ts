import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/shared/service/user.service';

@Component({
    selector: 'app-user-add',
    templateUrl: './user-add.component.html',
    styleUrls: ['./user-add.component.css'],
    providers: [UserService]
})
export class UserAddComponent implements OnInit {
    actionLoading: boolean = false;

    email: string = '';
    firstname: string = '';
    lastname: string = '';
    password: string = '';
    roles: string[] = [];

    constructor(
        private userService: UserService
    ) { }    

    ngOnInit() {
    }

    onChangeRole($event) {
        if ($event.target.checked) {
            this.roles.push('ROLE_ADMIN');
        } else {
            this.roles.splice(this.roles.indexOf('ROLE_ADMIN'), 1);
        }
    }

    createUser(userForm) {
        this.actionLoading = true;
        
        let newUser = {
            'email': this.email,
            'firstname': this.firstname,
            'lastname': this.lastname,
            'password': this.password,
            'roles': this.roles
        }

        this.userService.register(newUser).subscribe(data => {
            userForm.reset();
            
            this.actionLoading = false;
        });
    }
}
