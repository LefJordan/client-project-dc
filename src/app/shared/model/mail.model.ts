export class Mail {
    mail_to: string;
    subject: string;
    msg_txt: string;
    msg_html: string;

    constructor(obj?: any) {     
        this.mail_to = obj && obj.mail_to || null;
        this.subject = obj && obj.subject || null;
        this.msg_txt = obj && obj.msg_txt || null;
        this.msg_html = obj && obj.msg_html || null;        
    }
}