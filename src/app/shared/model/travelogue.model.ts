import {Team} from './team.model';
import {MediaObject} from './media-object.model';

export class Travelogue {
    id: number;
    name: string;
    description: string;
    pdfName: string;
    pdfLink: MediaObject;
    teams: Team[] = [];

    constructor(obj?: any) {
        this.id = obj && Number(obj.id) || null;
        this.name = obj && obj.name || null;
        this.description = obj && obj.description || null;
        this.pdfName = obj && obj.pdfName || null;
        this.pdfLink = obj && obj.pdfLink || null;
        this.teams = obj && obj.teams || null;
    }
}