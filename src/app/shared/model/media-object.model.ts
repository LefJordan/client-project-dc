export class MediaObject {
    id: number;
    filePath: string;

    constructor(obj?: any) {
        this.id = obj && Number(obj.id) || null;
        this.filePath = obj && obj.filePath || null;
    }
}