export class User {
    id: number;
    email: string;
    firstname: string;
    lastname: string;
    roles: string[] = [];

    constructor(obj?: any) {
        this.id = obj && Number(obj.id) || null;
        this.email = obj && obj.email || null;
        this.firstname = obj && obj.firstname || null;
        this.lastname = obj && obj.lastname || null;
        this.roles = obj && obj.roles || null;
    }
}