import {Travelogue} from './travelogue.model';
import {User} from './user.model';

export class Team {
    id: number;
    name: string;
    users: User[] = [];
    travelogue: Travelogue;

    constructor(obj?: any) {
        this.id = obj && Number(obj.id) || null;
        this.name = obj && obj.name || null;
        this.users = obj && obj.users || null;
        this.travelogue = obj && obj.travelogue || null;
    }
}