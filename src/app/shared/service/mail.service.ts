import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import config from '../../../assets/config.json';

const apiUrl = config.apiUrl;

@Injectable()
export class MailService {
    constructor(private http: HttpClient) { }

    sendMultiple(mail_request, token) {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.post(apiUrl + '/mails', mail_request, { headers });
    }
}