import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import config from '../../../assets/config.json';

const apiUrl = config.apiUrl;

@Injectable()
export class FileUploadService {

    constructor(private http: HttpClient) { }

    addFile(formData, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Authorization', token);

        return this.http.post(apiUrl + '/api/media_objects', formData, { headers });
    }

    deleteFile(file_id, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.delete(apiUrl + '/api/media_objects/' + file_id, { headers });
    }
}
