import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { UserService } from 'src/app/shared/service/user.service';
import { empty } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private router: Router, private userService: UserService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let user_email = localStorage.getItem('user_email');
        let user_token = localStorage.getItem('user_token');

        if (user_email && user_token) {
            return this.userService.getByEmail(user_email, user_token).pipe(
                map(returnedData => {
                    if(returnedData['hydra:member'].length > 0) {
                        let currentUser = returnedData['hydra:member'][0];

                        if (route.data.role && currentUser.roles.indexOf(route.data.role) === -1) {
                            this.router.navigate(['/']);
                            return false;
                        }
                        return true;
                    } else {
                        this.router.navigate(['/signin']);
                        return false;
                    }
                }),
                catchError((err, caught) => {
                    this.router.navigate(['/signin']);
                    return empty();
                })
            );
        } else {
            this.router.navigate(['/signin']);
            return false;
        }
    }
}