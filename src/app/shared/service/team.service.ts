import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import config from '../../../assets/config.json';

const apiUrl = config.apiUrl;

@Injectable()
export class TeamService {
    constructor(private http: HttpClient) { }

    addTeam(newTeam, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.post(apiUrl + '/api/teams', newTeam, { headers });
    }

    editTeam(editedTeam, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.put(apiUrl + '/api/teams/' + editedTeam.team_id, editedTeam, { headers });
    }

    deleteTeam(team_id, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.delete(apiUrl + '/api/teams/' + team_id, { headers });
    }

    get(team_id, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/teams/' + team_id, { headers });
    }

    getAll(token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/teams', { headers });
    }

    getByUser(user_id, token):any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/teams?users.id=' + user_id, { headers });
    }
}
