import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import config from '../../../assets/config.json';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

const apiUrl = config.apiUrl;

@Injectable()
export class AuthService {
    constructor(
        private http: HttpClient
    ) {}

    login(userCredentials): Observable<any> {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');

        return this.http.post(apiUrl + '/login', userCredentials, { headers }).pipe(catchError(this.handleError));
    }

    handleError(error: HttpErrorResponse) {
        return throwError(
          {
              'code': error.status,
              'message': error.error.message
          });
      };
}