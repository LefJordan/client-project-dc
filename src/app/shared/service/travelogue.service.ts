import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import config from '../../../assets/config.json';

const apiUrl = config.apiUrl;

@Injectable()
export class TravelogueService {

    constructor(private http: HttpClient) { }

    addTravelogue(newTravelogue, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.post(apiUrl + '/api/travelogues', newTravelogue, { headers });
    }

    editTravelogue(editedTravelogue, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.put(apiUrl + '/api/travelogues/' + editedTravelogue.travelogue_id, editedTravelogue, { headers });
    }

    deleteTravelogue(travelogue_id, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.delete(apiUrl + '/api/travelogues/' + travelogue_id, { headers });
    }

    get(travelogue_id, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/travelogues/' + travelogue_id, { headers });
    }

    getAll(token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/travelogues', { headers });
    }

    getByUser(user_id, token):any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/travelogues?teams.users.id=' + user_id, { headers });
    }
}
