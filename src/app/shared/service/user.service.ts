import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import config from '../../../assets/config.json';

const apiUrl = config.apiUrl;

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    register(newUser):any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/x-www-form-urlencoded');

        let params = new HttpParams();
        params = params.append('email', newUser.email);
        params = params.append('firstname', newUser.firstname);
        params = params.append('lastname', newUser.lastname);
        params = params.append('password', newUser.password);

        return this.http.post(apiUrl + '/register', params.toString(), { headers });
    }

    editUser(editedUser, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.put(apiUrl + '/api/users/' + editedUser.user_id, editedUser, { headers });
    }

    deleteUser(user_id, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.delete(apiUrl + '/api/users/' + user_id, { headers });
    }

    get(user_id, token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/users/' + user_id, { headers });
    }

    getAll(token): any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/users', { headers });
    }

    getByEmail(email, token):any {
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/json');
        headers = headers.append('Authorization', token);

        return this.http.get(apiUrl + '/api/users?email=' + email, { headers });
    }
}
