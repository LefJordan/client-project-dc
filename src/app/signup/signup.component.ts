import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../shared/service/user.service';
import { AuthService } from '../shared/service/auth.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.css'],
    providers: [UserService, AuthService]
})
export class SignupComponent implements OnInit {
    actionLoading: boolean = false;

    email: string = '';
    firstname: string = '';
    lastname: string = '';
    password: string = '';
    
    constructor(
        private router: Router,
        private userService: UserService,
        private authService: AuthService
    ) { }    

    ngOnInit() {
    
    }

    signup() {
        this.actionLoading = true;
        
        let newUser = {
            'email': this.email,
            'firstname': this.firstname,
            'lastname': this.lastname,
            'password': this.password
        }

        this.userService.register(newUser).subscribe(data => {
            this.authService.login({'email': newUser.email, 'password': newUser.password}).subscribe(data => {
                localStorage.setItem('user_email', newUser.email);
                localStorage.setItem('user_token', 'Bearer ' + data.token);

                this.router.navigate(['/']);
            });
            
            this.actionLoading = false;
        });
    }
}
